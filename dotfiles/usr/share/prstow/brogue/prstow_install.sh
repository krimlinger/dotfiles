#!/usr/bin/env sh

# Preset to create a stow package for Brogue Community Edition.

set -e

install_dir="$1/bin"

patch -N -r- config.mk < "$2/config.mk.patch" || true
make bin/brogue
install -vd "$install_dir"
cp -vR ./bin/* "$install_dir"
