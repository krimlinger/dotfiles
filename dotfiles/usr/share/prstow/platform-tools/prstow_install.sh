#!/usr/bin/env sh

# Preset to create a stow package for grapheneos' platform-tools.

set -e

install_dir="$1/bin"

install -vd "$install_dir"
cp -Rv ./* "$install_dir"
