#!/usr/bin/env python3

# Copyright (C) 2017-2020 Ken Rimlinger.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This script can be used to onfigure an encrypted debian system so it can be
decrypted using a USB key.

Here is a quick how-to to decript /dev/sda1 with a USB device /dev/sdb1
mounted on /mnt.

1. Create your usb key:
     # mkfs.ext4 -L mylabel /dev/sdb1

1. Mount your usb key:
     # mount /dev/sdb1 /mnt

2. Create a luks keyfile on the key and add it to your list of authorized
   device unlock mecanisms (luksAddKey):
     # debkeyscript create /dev/sda1 /mnt/keydir/mykeyfile.key

3. Configure your cryptsetup and initramfs:
     # debkeyscript configure /dev/sdb1 keydir/mykeyfile.key

4. Check for the configuration of /etc/crypttab if your partition table is
   complex to avoid any bad surprise.

5. Check that your initramfs provides passdev because without it, you will
   not open your filesystem:
     $ lsinitramfs /boot/initrd.img-5.5.0-2-amd64 | grep passdev
"""

import os
import subprocess

import click


# Default configuration options
MODULES = {"ext4", "btrfs"}
KEYSCRIPT_PATH = "/lib/cryptsetup/scripts/passdev"
KEYFILE_SIZE = 256

CRYPTTAB_PATH = "/etc/crypttab"
MODULES_PATH = "/etc/initramfs-tools/modules"


def is_comment(line):
    return line.lstrip().startswith("#")


def update_initramfs_modules(required_modules):
    click.echo('Updating "{}" list of modules...'.format(MODULES_PATH))
    with click.open_file(MODULES_PATH) as f:
        modules = {m.strip() for m in f.readlines() if not is_comment(m)}
    with click.open_file(MODULES_PATH, "a") as f:
        for module in required_modules - modules:
            print(module, file=f)
            click.echo('Added "{}" module to initramfs'.format(module))


def update_initramfs():
    click.echo("Updating initramfs...")
    subprocess.run(["update-initramfs", "-k", "all", "-u"], check=True)


def get_uuid_from_device_path(device_path):
    sub = subprocess.run(
        ["blkid", device_path, "-s", "UUID", "-o", "value"],
        check=True,
        capture_output=True,
    )
    uuid = sub.stdout.decode(encoding="utf-8").strip()
    if not uuid:
        click.echo('Could not find uuid for device "{}"'.format(device_path))
        raise click.Abort()
    return uuid


def update_crypttab(device_path, keyfile_path, keyscript_path):
    click.echo("Trying to find luks volumes to add the keyscript to")
    is_crypttab_updated = False
    with click.open_file(CRYPTTAB_PATH) as f:
        new_crypttab = []
        for line in f:
            new_crypttab.append(line.rstrip())
            if is_comment(line):
                continue
            try:
                target, source, keyfile, options = line.split()
            except ValueError:
                continue
            if keyfile not in ("none", "-"):
                if not click.confirm(
                    'Warning: keyfile field in "{}" is not '
                    "none or -. Overwrite it ?".format(CRYPTTAB_PATH)
                ):
                    raise click.Abort()
            keyfile_uuid = get_uuid_from_device_path(device_path)
            keyfile = "/dev/disk/by-uuid/{}:{}".format(keyfile_uuid, keyfile_path)
            options = options.split(",")
            options_count = len(options)
            options = [x for x in options if not x.startswith("keyscript=")]
            if options_count != len(options):
                if not click.confirm(
                    'Warning: "keyscript" found in options '
                    'field of "{}". Overwrite it ?'.format(CRYPTTAB_PATH)
                ):
                    raise click.Abort()
            options.append("keyscript={}".format(keyscript_path))
            new_crypttab[-1] = " ".join([target, source, keyfile, ",".join(options)])
            # Warning: update-initramfs seems to be really sensitive to any
            # lack of endline character in crypttab.
            new_crypttab[-1] += "\n"
            is_crypttab_updated = True
    if is_crypttab_updated:
        with click.open_file(CRYPTTAB_PATH, "w") as f:
            f.write("\n".join(new_crypttab))
        click.echo("{} was updated. Please check its content".format(CRYPTTAB_PATH))


def add_keyfile_to_device(device, keyfile):
    click.echo('Adding keyfile "{}" to device "{}"...'.format(keyfile, device))
    subprocess.run(["cryptsetup", "luksAddKey", device, keyfile], check=True)


def create_keyfile(keyfile, keyfile_size):
    with click.open_file(keyfile, "wb") as f:
        f.write(os.urandom(keyfile_size))
    click.echo('Created keyfile "{}"'.format(keyfile))


@click.group()
def cli():
    """debkeyscript helps you create keys on USB drives and automatically
       mount LUKS encrypted filesystems using said drive.

       By default a key file of 256 bytes will becreated.
       The default keyscript used to unlock the filesystem with the USB
       drive is /lib/cryptsetup/scripts/passdev.
       If you want to change any of that, use the related command's options."""
    pass


@cli.command()
@click.argument("device", type=click.Path(exists=True))
@click.argument("keyfile-path", type=click.Path())
@click.option(
    "-s",
    "--keyfile-size",
    default=KEYFILE_SIZE,
    show_default=True,
    help="keyfile size in bits"
)
def create(device, keyfile_path, keyfile_size):
    """Create a keyfile at KEYFILE_PATH and add it to the list of authorized
    keys of DEVICE."""
    if os.path.exists(keyfile_path):
        if not click.confirm(
            'Warning: the keyfile "{}" already exists. '
            "Overwrite it ?".format(keyfile_path)
        ):
            raise click.Abort()
    create_keyfile(keyfile_path, keyfile_size)
    add_keyfile_to_device(device, keyfile_path)


@cli.command()
@click.argument("device", type=click.Path(exists=True))
@click.argument("relative-keyfile-path", type=click.Path())
@click.option(
    "-k",
    "--keyscript-path",
    default=KEYSCRIPT_PATH,
    show_default=True,
    type=click.Path(exists=True),
    help="path to the keyscript used to unlock the filesystem."
)
@click.option(
    "-m",
    "--module",
    default=MODULES,
    show_default=True,
    multiple=True,
    help="add a module to initramfs. this option can be repeated."
)
def configure(device, relative_keyfile_path, keyscript_path, module):
    """Use the UUID of DEVICE and the keyfile located (relatively to the
       mounted DEVICE) at RELATIVE_KEYFILE_PATH to open the encrypted
       filesystem.

       This command will update the initramfs modules so that your system will
       be able to handle the USB drive at early boot (initramfs). It will also
       try to update your crypttab accordingly to mount your USB drive
       and retrieve the keyfile.
    """
    update_initramfs_modules(set(module))
    update_crypttab(device, relative_keyfile_path, keyscript_path)
    update_initramfs()


if __name__ == "__main__":
    cli()
