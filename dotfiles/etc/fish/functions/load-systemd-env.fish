function load-systemd-env
  for c in $XDG_CONFIG_HOME/environment.d/*
    export (envsubst < $c | grep -v '^#' | grep -v '^$')
  end
  set -gx SYSTEMD_ENVIRONMENTD_LOADED 1
end

function load-systemd-env-legacy
  systemctl --user show-environment | while read -ld '=' varname varval
      # Todo: maybe do not update arrays by checking with 'set -q VAR[2]'
      if [ $varname  = 'PATH' ]
        set -gx PATH (string split ':' $varval)
        continue
      end
      set -gx $varname $varval
    end

  set -gx SYSTEMD_ENVIRONMENTD_LOADED 1
end
