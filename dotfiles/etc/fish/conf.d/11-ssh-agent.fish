# Do not use gpg-agent if gnome-keyring is running
if [ -n "$SSH_AUTH_SOCK" ]
  exit 0
end

set -xg SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
set -xg GPG_TTY (tty)

gpg-connect-agent updatestartuptty /bye > /dev/null
