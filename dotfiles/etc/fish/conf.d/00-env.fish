# If environment variables are inherited from systemd, do nothing.
if set -q SYSTEMD_ENVIRONMENTD_LOADED
  exit 0
end

# Else, load environment variables from environment.d directory.
load-systemd-env
