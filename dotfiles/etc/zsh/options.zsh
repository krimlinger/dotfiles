set_opts() {
    changedir=(
        autocd               # /path/to/dir <=> cd /path/to/dir
        autopushd            # cd path <=> pushd $PWD && cd
        cdablevars           # cd var <=> cd $var
        pushdignoredups      # do not pushd duplicate paths
        pushdminus           # cd -N goes to dirs number N instead of cd +N
        pushdsilent          # pushd and popd do not print anything
        pushdtohome)         # pushd <=> pushd $HOME
    expansion=(nomatch)
    completion=(
        autolist             # list choice on ambiguous completion
        completealiases      # complete aliases as commands
        completeinword       # complete from both ends of words
        menucomplete)        # insert first choice on ambiguous completion
    historyopts=(
        appendhistory        # append history instead of overwriting it
        extendedhistory      # use extended history format
        incappendhistorytime # update history once command exits
        histexpiredupsfirst  # remove oldest duplicate if trimming internal history
        histfindnodups       # no duplicate while searching history
        histignorealldups    # remove oldest duplicate
        histignoredups       # do not record duplicated event
        histignorespace      # do not record event starting with a space
        histsavenodups       # do not write duplicated event to history
        histreduceblanks     # remove useless blanks from history
        histverify)          # history expansion doesn't run the command
    inputoutput=(
        interactivecomments  # allow comments in interactive mode
        mailwarning          # updated mail files print a warning
        normstarsilent       # rm * commands ask for confirmation
        rmstarwait)          # rm * commands wait 10 seconds before execution
    setopt $changedir $expansion $completion $historyopts $inputoutput nobeep
}

set_opts
