zstyle ':completion:*' completer _expand _complete _ignored _approximate _prefix
zstyle ':completion:*' menu select
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'l:|=* r:|=*'
zstyle ':completion:*' verbose true
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache
zstyle :compinstall filename "$XDG_CONFIG_HOME/zsh/completion.zsh"

bindkey -v
bindkey '^R' history-incremental-search-backward
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

zle -N edit-command-line
bindkey -M vicmd v edit-command-line

compdef '_path_files -/ -W "$XDG_DATA_HOME/prstow"' prstow
compdef '_path_files -W "$XDG_DATA_HOME/css-theme-injector/css"' \
    desktop-css-theme-update
