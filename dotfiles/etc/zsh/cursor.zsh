cursor_mode() {
    beam_cursor='\e[6 q'
    block_cursor='\e[2 q'

    function zle-keymap-select {
        case $KEYMAP in
            vicmd)
                echo -ne $block_cursor;;
            main | viins | '')
                echo -ne $beam_cursor;;
            *)
                [[ $1 == 'beam' ]] && echo -ne $cursor_beam;
                [[ $1 == 'block' ]] && echo -ne $block_cursor;;
        esac
    }

    zle-line-init() {
        echo -ne $beam_cursor
    }

    zle -N zle-keymap-select
    zle -N zle-line-init
}

cursor_mode
