alias d='dirs -v'
alias e='exa -l'
alias v=nvim

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

theme_dir="${XDG_CONFIG_HOME}/theme"

reset_old_theme () {
    kill -USR1 "$$"
}

change_term_color () {
    printf "\033]11;"
    head -n 1 "$theme_dir/$1"_color | tr -d '\n '
    printf "\033\\"
}

sudo () {
    change_term_color sudo
    /usr/bin/sudo "$@"
    reset_old_theme
}

ssh () {
    change_term_color ssh
    /usr/bin/ssh "$@"
    reset_old_theme
}

yy() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}
