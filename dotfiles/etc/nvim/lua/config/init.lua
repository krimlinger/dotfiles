require('config.options')
require('config.remap')
require('config.lazy-bootstrap')

require('lazy').setup('plugins', {
    install = {
        colorscheme = { 'catppuccin' }
    }
})
