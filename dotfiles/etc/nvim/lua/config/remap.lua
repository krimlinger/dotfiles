vim.g.mapleader = ' '
vim.g.netrw_liststyle = 3
vim.keymap.set('n', '<leader>ft', ':15Lexplore<CR>',
    { desc = 'Open Netrw tree' })
vim.keymap.set('n', '<leader>fe', vim.cmd.Oil,
    { desc = 'Open Oil file explorer' })
vim.keymap.set('n', '<leader>fn', ':ene | startinsert<CR>',
    { desc = 'Start writing a new file' })

local function switchSpellChecking(lang)
    return function()
        if lang == 'fr' or lang == 'en' then
            vim.opt_local.spell = true
            vim.opt_local.spelllang = lang
        else
            vim.opt_local.spell = false
        end
    end
end

vim.keymap.set('n', '<leader>Sq', switchSpellChecking(nil),
    { desc = 'Quit spell checking mode' })
vim.keymap.set('n', '<leader>Se', switchSpellChecking('en'),
    { desc = 'Check english spelling' })
vim.keymap.set('n', '<leader>Sf', switchSpellChecking('fr'),
    { desc = 'Check french spelling' })
