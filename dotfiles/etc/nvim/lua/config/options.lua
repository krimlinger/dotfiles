local options = {
    guicursor = '',

    number = true,
    relativenumber = true,
    ruler = true,
    cursorline = true,

    textwidth = 79,

    expandtab = true,
    shiftwidth = 4,
    softtabstop = 4,
    tabstop = 4,

    smartindent = true,
    hlsearch = false,
    incsearch = true,
    ignorecase = true,
    smartcase = true,

    wrap = false,

    foldmethod = 'expr',
    foldexpr = 'nvim_treesitter#foldexpr()',
    foldenable = false,

    backup = false,
    swapfile = false,
    undofile = true,
    undodir = vim.fn.stdpath('cache') .. '/undodir',

    termguicolors = true,

    scrolloff = 8,
    signcolumn = 'yes',

    updatetime = 50,

    colorcolumn = '79',
}

for k, v in pairs(options) do
    vim.opt[k] = v
end

vim.opt.isfname:append('@-@')
