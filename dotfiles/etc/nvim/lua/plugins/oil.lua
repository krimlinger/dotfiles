return {
    'stevearc/oil.nvim',
    opts = {
        keymaps = {
            ["g?"] = "actions.show_help",
            ["g."] = "actions.toggle_hidden",
            ["<CR>"] = "actions.select",
            ["<C-s>"] = "actions.select_vsplit",
            ["<C-h>"] = "actions.select_split",
            ["<C-t>"] = "actions.select_tab",
            ["<C-p>"] = "actions.preview",
            ["<C-c>"] = "actions.close",
            ["<C-l>"] = "actions.refresh",
            ["-"] = "actions.parent",
            ["_"] = "actions.open_cwd",
            ["cd"] = "actions.cd",
            ["tcd"] = "actions.tcd",
        }
    },
    dependencies = { "nvim-tree/nvim-web-devicons" },
}
