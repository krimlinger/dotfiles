return {
    'neovim/nvim-lspconfig',
    dependencies = {
        {
            'williamboman/mason.nvim',
            build = function()
                pcall(vim.api.nvim_command, 'MasonUpdate')
            end,
        },
        { 'williamboman/mason-lspconfig.nvim' },
    },
    init = function()
        local lspconfig = require('lspconfig')
        local lsp_defaults = lspconfig.util.default_config

        lsp_defaults.capabilities = vim.tbl_deep_extend(
            'force',
            lsp_defaults.capabilities,
            require('cmp_nvim_lsp').default_capabilities()
        )

        vim.api.nvim_create_autocmd('LspAttach', {
            desc = 'LSP actions',
            callback = function(event)
                local opts = { buffer = event.buf }

                vim.keymap.set('n', 'K',
                    '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
                vim.keymap.set('n', 'gd',
                    '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
                vim.keymap.set('n', 'gD',
                    '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
                vim.keymap.set('n', 'gi',
                    '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)
                vim.keymap.set('n', 'go',
                    '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)
                vim.keymap.set('n', 'gr',
                    '<cmd>lua vim.lsp.buf.references()<cr>', opts)
                vim.keymap.set('n', 'gs',
                    '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
                vim.keymap.set('n', '<F2>',
                    '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
                vim.keymap.set({ 'n', 'x' }, '<F3>',
                    '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
                vim.keymap.set('n', '<F4>',
                    '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)

                vim.keymap.set('n', 'gl',
                    '<cmd>lua vim.diagnostic.open_float()<cr>', opts)
                vim.keymap.set('n', '[d',
                    '<cmd>lua vim.diagnostic.goto_prev()<cr>', opts)
                vim.keymap.set('n', ']d',
                    '<cmd>lua vim.diagnostic.goto_next()<cr>', opts)
            end
        })

        require('mason').setup()
        -- this requires rebar3, ghcup, opam installed and configured
        --require('mason-lspconfig').setup({
        --    ensure_installed = {
        --        'awk_ls',
        --        'ansiblels',
        --        'bashls',
        --        'clangd',
        --        'unocss',
        --        'dockerls',
        --        'eslint',
        --        'elixirls',
        --        'elmls',
        --        'erlangls',
        --        'golangci_lint_ls',
        --        'graphql',
        --        'html',
        --        'hls',
        --        'jsonls',
        --        'pylsp',
        --        'quick_lint_js',
        --        'texlab',
        --        'lua_ls',
        --        'marksman',
        --        'ocamllsp',
        --        'rust_analyzer',
        --        'sqlls',
        --        'taplo',
        --        'lemminx',
        --        'yamlls',
        --    }
        --})

        require('mason-lspconfig').setup_handlers({
            function(server_name)
                lspconfig[server_name].setup({
                    capabilities = lsp_defaults.capabilities,
                })
            end,
        })

        -- configure lua lsp to work well with nvim lua config and library
        local runtime_path = vim.split(package.path, ';')
        for _, p in ipairs({ 'lua/?.lua', 'lua/?/init.lua' }) do
            table.insert(runtime_path, p)
        end
        lspconfig.lua_ls.setup({
            settings = {
                Lua = {
                    telemetry = { enable = false },
                    runtime = {
                        version = 'LuaJIT',
                        path = runtime_path,
                    },
                    diagnostics = {
                        globals = { 'vim' }
                    },
                    workspace = {
                        checkThirdParty = false,
                        library = {
                            vim.fn.expand('$VIMRUNTIME/lua'),
                            vim.fn.stdpath('config') .. '/lua'
                        }
                    }
                }
            }
        })
    end
}
