return {
    'catppuccin/nvim',
    -- optional themes
    dependencies = {
        {
            "ellisonleao/gruvbox.nvim",
            priority = 1000
        }
    },
    name = 'catppuccin',
    priority = 1000,
    opts = {
        flavour = 'macchiato', -- latte, frappe, macchiato, mocha
        background = {
            light = 'latte',
            dark = 'macchiato',
        }
    },
    init = function()
        local conf = tostring(vim.fn.stdpath('config'))
        local file = io.open(vim.fs.dirname(conf) .. '/theme/current', 'r')
        if file ~= nil then
            local cs = file:read()

            file:close()
            vim.cmd.colorscheme(cs)
        else
            vim.cmd.colorscheme('catppuccin')
        end
    end
}
