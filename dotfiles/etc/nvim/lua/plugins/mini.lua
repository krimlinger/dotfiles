return {
    'echasnovski/mini.nvim',
    version = false,
    config = function ()
        for _, plugin in ipairs({'pairs', 'statusline'}) do
            require('mini.' .. plugin).setup()
        end
    end
}
