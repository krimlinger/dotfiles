return {
    'goolord/alpha-nvim',
    opts = function()
        local d = require('alpha.themes.dashboard')
        local handle = io.popen('hostname | figlet -f smslant')
        if handle ~= nil then
            local hostname = handle:read("*a")
            if hostname ~= nil and hostname ~= '' then
                d.section.header.val = vim.split(hostname, '\n')
            end
            handle:close()
        end
        local luadir = vim.fn.stdpath('config') .. '/lua'
        d.section.buttons.val = {
            d.button('f', '  ' .. 'Find file', ':Telescope find_files<CR>'),
            d.button('n', '  ' .. 'New file', ':ene | startinsert<CR>'),
            d.button('r', '  ' .. 'Recent files', ':Telescope oldfiles<CR>'),
            d.button('g', '  ' .. 'Find text', ':Telescope live_grep<CR>'),
            d.button('c', '  ' .. 'Config', ':cd ' .. luadir .. ' | e .<CR>'),
            d.button('l', '󰒲  ' .. 'Lazy', ':Lazy<CR>'),
            d.button('q', '  ' .. 'Quit', ':qa<CR>'),
        }
        return d.config
    end
}
