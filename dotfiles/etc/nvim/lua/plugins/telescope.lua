return {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.2',
    dependencies = { 'nvim-lua/plenary.nvim' },
    init = function()
        local builtin = require('telescope.builtin')
        vim.keymap.set('n', '<leader>ff', builtin.find_files,
            { desc = 'Find a file in current directory' })
        vim.keymap.set('n', '<leader>fg', builtin.git_files,
            { desc = 'Find a file in git index or git directory' })
        vim.keymap.set('n', '<leader>fr', builtin.oldfiles,
            { desc = 'Find recently opened files' })
        vim.keymap.set('n', '<leader>sg', builtin.live_grep,
            { desc = 'Search interactively for a string' })
        vim.keymap.set('n', '<leader>sm', builtin.man_pages,
            { desc = 'Search for a man page' })
        vim.keymap.set('n', '<leader>sw', builtin.grep_string,
            { desc = 'Search for string under the cursor' })
    end
}
