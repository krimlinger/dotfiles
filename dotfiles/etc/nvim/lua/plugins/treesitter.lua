return {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = function()
        local configs = require('nvim-treesitter.configs')
        configs.setup({
            ensure_installed = {
                'bash',
                'c',
                'cmake',
                'cpp',
                'css',
                'diff',
                'elm',
                'erlang',
                'git_config',
                'git_rebase',
                'go',
                'graphql',
                'html',
                'javascript',
                'json',
                'latex',
                'lua',
                'luadoc',
                'meson',
                'nix',
                'ocaml',
                'passwd',
                'python',
                'regex',
                'rust',
                'scss',
                'sql',
                'toml',
                'vimdoc',
                'yaml',
            },

            -- Install parsers synchronously (only applied to `ensure_installed`)
            sync_install = false,

            -- Automatically install missing parsers when entering buffer
            auto_install = true,

            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
            },
            indent = {
                enable = true
            }
        })
    end
}
