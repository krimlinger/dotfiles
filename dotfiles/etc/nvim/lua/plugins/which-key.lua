return {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    opts = {
        key_labels = { ['<leader>'] = 'SPC' }
    },
    init = function()
        local wk = require('which-key')
        wk.register({
            mode = { 'n', 'v' },
            ['<leader>c'] = { name = '+code' },
            ['<leader>f'] = { name = '+file' },
            ['<leader>g'] = { name = '+git' },
            ['<leader>S'] = { name = '+spell' },
            ['<leader>s'] = { name = '+search' },
            ['<leader>u'] = { name = '+undo' },
        })
    end,
}
