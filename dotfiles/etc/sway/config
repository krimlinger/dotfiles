set $mod Mod4
set $left h
set $down j
set $up k
set $right l

set $term footclient
set $browser firefox-esr
set $menu wofi | xargs swaymsg exec --
set $lock swaylock -f -c 000000

set $ws1 1: web
set $ws2 2: term
set $ws3 3: prog
set $ws4 4: chat
set $ws5 5: misc
set $ws6 6: games

include ~/.config/sway/`hostname`

default_border pixel 3
hide_edge_borders smart
gaps inner 10px
floating_modifier $mod normal

exec wlsunset
exec $browser
exec swayidle -w \
         timeout  300 $lock \
         timeout  400 'swaymsg "output * dpms off"' \
               resume 'swaymsg "output * dpms on"' \
         before-sleep $lock
#exec fctix5 -dr
exec fcitx5
exec foot --server

for_window [app_id="^mpv$"] \
                   move to workspace $ws1, floating enable, sticky enable
for_window [app_id="^anki$"] \
                   move to workspace $ws4, layout tabbed
for_window [class="^Anki$"] \
                   move to workspace $ws4, layout tabbed
for_window [title="^Steam - News"] kill
for_window [title="^Friends List$"] resize set width 20 ppt
for_window [title="^STG::"] move to workspace $ws4, fullscreen enable
for_window [app_id="^Firefox(-esr)?$" title="^Incrustation vidéo"] \
                   move to workspace $ws1, floating enable, sticky enable, \
                   resize set width 700 height 394, \
                   move position 1215 0
for_window [class="^(FreeTube|streamlink-twitch-gui)$"] \
                   move to workspace $ws4, layout tabbed

assign [class="^Firefox$"] $ws1
assign [class="^Steam$"] $ws6

bindsym $mod+d exec $menu
bindsym $mod+Return exec $term
bindsym $mod+n exec $browser
bindsym $mod+p exec $term -e nnn
bindsym $mod+i exec $lock
bindsym Control+Space exec makoctl dismiss
bindsym Control+Shift+Space exec makoctl dismiss --all

# bindsym $mod+Shift+z exec ibus engine anthy
# bindsym $mod+Shift+x exec ibus engine xkb:us:altgr-intl:eng
bindsym $mod+Shift+q kill
bindsym $mod+Shift+c exec ~/bin/desktop-css-theme-update
bindsym $mod+Shift+e exec echo 'No\nYes' | \
                          wofi -S dmenu -p 'Do you want to exit sway ?' | \
                          grep -q 'Yes' && swaymsg exit
bindsym $mod+Shift+s exec echo | \
                          wofi -S dmenu -p 'Seconds before powering off' | \
                          xargs -I % sh -c '[ 0 -lt % ] && \
                            notify-send \
                              "Planned system shutdown" \
                              "The system will power off in % seconds." && \
                            sleep % && \
                            systemctl poweroff'

bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6

bindsym $mod+b splith
bindsym $mod+v splitv

bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

bindsym $mod+f fullscreen

bindsym $mod+Shift+space floating toggle
bindsym $mod+space focus mode_toggle
bindsym $mod+a focus parent

bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

bindsym $mod+r mode "resize"

mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bar {
  swaybar_command waybar
  #swaybar_command waybar -c ~/.config/waybar/`hostname`
}

include /etc/sway/config.d/*
