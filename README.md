# dotfiles

Personal dotfiles managed with `stow`.

## Hierarchy

Overview of the hierarchy of the home directory and the dotfiles.

```
~
├── bin       # Scripts and executables
│   ├── theme
│   ├── init-home-dir
│   └── ...
├── etc       # Configuration files for programs
│   ├── environment.d    # User environment vars, sourced by the shell/wayland
│   │   ├── 90-xdg-dirs.conf
│   │   ├── 91-userpref.conf
│   │   ...
│   └── zsh
├── usr
│   ├── img   # Images
│   ├── msc   # Music
│   ├── share # $XDG_DATA_HOME / static programs content
│   │   ├── ghcup
│   │   │   └── ghc
│   │   │       └ 9.2.8
│   │   │         ├ bin
│   │   │         │ ├ ...
│   │   │         │ └ ghc
│   │   │         ├ lib
│   │   │         └ share
│   │   ├────── ...
│   │   └────── nvim
│   ├── ...   # Some other directories where documents are stored
│   ├── src   # Dev / software source code
│   ├── tpl   # Templates
│   └── vid   # Videos
├── tmp       # Temporary files, also used as "desktop" directory
└── var
    ├── cache # $XDG_CACHE_HOME
    ├── dl    # Dowloaded content
    ├── lib   # $XDG_STATE_HOME / log/state files for programs
    ├── mail  # Mail spool directory
    └── pub   # Shared / public content
```

## Installing

All the dotfiles reside in the `dotfiles` directory or, to use stow's terms,
the `dotfiles` package. The package contains three main directories: `bin` for
scripts and other executables, `etc` for the main configuration files and `usr`
mainly for the css-theme-injector script.

Installing the package is really simple:

```sh
$ git clone --recurse-submodules https://gitlab.com/krimlinger/dotfiles
$ cd dotfiles
$ stow dotfiles
```

You then might want to update some symlinks to allow default XDG base
directories and custom ones to coexist or you might want to create the custom
XDG user-dirs or do some tree unfolding to allow some of the stowed paths to be
used like a normal directory. This can be automatically done by running the
`dotfiles/bin/init-home-dir` script.

```sh
$ ~/bin/init-home-dir
```

If you want to run stow in dry-run mode to see what will happen, use the `-n`
option:

```sh
$ stow -n dotfiles
```

By default, stow will symlink the package(s)'s content to your home directory.
Some of stow's default command line options were changed to make it easier to
work with dotfiles. To see the list of modified options refer to the
`etc/.stowrc` file. If you need to change stow's default command line options,
invoke it with your custom options to override this behaviour. 

To change the target directory to `/tmp`, make the output non-verbose and stow
the `dotfiles` package from any directory, you can for example use the
following command:

```sh
$ stow --dir ~/THIS-GIT-REPOSITORY-PATH --target /tmp --verbose=0 dotfiles
```

## Updating

If the configuration changes, you may need to restow to update your symlinks:

```sh
$ stow -R dotfiles
```

## Uninstalling

Uninstalling also is really simple:

```sh
$ stow -D dotfiles
```
